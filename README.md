# This Is ngStepwise
This plugin is the designed to:
- Provide an easy to setup wizard progress view i.e. the "This is where you are" and "here's how much more stuff you need to do" type views on websites 
- Requires AngularJS (it's an AngularJS directive)
- Requires Bootstrap for the tooltip functionality 

## Limitations
If you have more than 6 or so steps in your workflow, you probably need to do something differently on your UI than trying to cram all of them into a slider bar. 

This tool is still super new. As I build on it, I'll be looking to add more functionality and handle more than a few steps